package main

import (
	"geek-go/webook/internal/web"
	"github.com/gin-gonic/gin"
)

func main() {
	server := gin.Default()

	u := &web.UserHandler{}
	u.RegisterUserRouters(server)

	err := server.Run(":8080")

	if err != nil {
		panic(err)
	}
}
