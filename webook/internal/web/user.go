package web

import "github.com/gin-gonic/gin"

type UserHandler struct {
}

func (receiver *UserHandler) RegisterUserRouters(server *gin.Engine) {
	group := server.Group("/users")
	group.POST("/signup", receiver.SignUp)
	group.POST("/login", receiver.Login)
	group.POST("/edit", receiver.Edit)
	group.GET("/profile", receiver.Profile)
}

func (receiver *UserHandler) SignUp(context *gin.Context) {

}

func (receiver *UserHandler) Login(context *gin.Context) {

}

func (receiver *UserHandler) Edit(context *gin.Context) {

}

func (receiver *UserHandler) Profile(context *gin.Context) {

}
